const express = require("express");
const router = express.Router();

const { getCards, saveCard } = require("../controllers/card.controller");

router.get("/", function (req, res, next) {
  res.status(200).json({ text: "Hello Umesh!" });
});
router.get("/cards", getCards);
router.post("/card", saveCard);

module.exports = router;
