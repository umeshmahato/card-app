const db = require("../config/db");

const fetchAllCardAccounts = async () => {
  return new Promise((resolve) => {
    db.find({})
      .sort({ created_at: -1 })
      .exec(function (err, docs) {
        if (err) resolve({ error: err.message });
        resolve({ response: docs });
      });
  });
};

const saveCardAccount = async ({ cardBody }) => {
  const record = { ...cardBody, balance: 0, created_at: new Date() };
  return new Promise((resolve) => {
    db.insert(record, function (err, newDoc) {
      if (err) resolve({ error: err.message });
      resolve({ response: newDoc._id });
    });
  });
};

module.exports = {
  fetchAllCardAccounts,
  saveCardAccount,
};
