const Joi = require("joi");

const cardAccountSchema = Joi.object({
  name: Joi.string().min(1).max(30).required(),
  card_number: Joi.string()
    .alphanum()
    .max(19)
    .required()
    .custom((value, helper) => {
      return checkLuhn(value)
        ? value
        : helper.message("\"card_number\" is not valid");
    }),
  limit: Joi.number().min(1).max(1000000).required(),
});

const checkLuhn = (cardNo) => {
  if (!cardNo?.length) return false;

  const numberArr = [...cardNo];
  let sum = 0;

  numberArr.reverse().forEach((el, index) => {
    if (index % 2 == 0) sum += Number(el);
    else {
      const doubledVal = Number(el) * 2;
      sum += parseInt(doubledVal / 10) + (doubledVal % 10);
    }
  });
  return sum % 10 == 0;
};

module.exports = {
  cardAccountSchema,
  checkLuhn,
};
