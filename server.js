require("dotenv").config();
const server_port = process.env.CONFIG_PORT;
const app = require("./app");

app.listen(server_port, () => {
  console.log(`Server has started on port: ${server_port}`);
});
