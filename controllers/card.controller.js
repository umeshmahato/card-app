const {
  fetchAllCardAccounts,
  saveCardAccount,
} = require("../services/card.service");
const { cardAccountSchema } = require("../utils");

const getCards = async (req, res, next) => {
  const { response, error } = await fetchAllCardAccounts();
  return res.status(error ? 500 : 200).json({
    status: !error,
    error,
    response,
  });
};

const saveCard = async (req, res, next) => {
  const { body: cardBody } = req;

  try {
    await cardAccountSchema.validateAsync(cardBody);
  } catch (e) {
    return res.status(400).json({
      status: false,
      error: e.message,
    });
  }

  const { response, error } = await saveCardAccount({ cardBody });
  return res.status(error ? 500 : 200).json({
    status: !error,
    error,
    response,
  });
};

module.exports = {
  getCards,
  saveCard,
};
