const { checkLuhn } = require("../../utils");

describe("User model", () => {
  let cardNumber;

  beforeEach(() => {});

  test("should pass the luhn 10 valdation", async () => {
    cardNumber = "79927398713";
    expect(checkLuhn(cardNumber)).toBe(true);
  });

  test("should fail the luhn 10 valdation", async () => {
    cardNumber = "79927398715";
    expect(checkLuhn(cardNumber)).not.toBe(true);
  });

  test("should fail the luhn 10 valdation", async () => {
    cardNumber = "garbageString";
    expect(checkLuhn(cardNumber)).not.toBe(true);
  });
});
