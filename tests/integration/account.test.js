const request = require("supertest");
const httpStatus = require("http-status");
const app = require("../../app");
describe("User routes", () => {
  describe("POST /v1/cards", () => {
    test("should return 200 and fetch list of card accounts", async () => {
      const res = await request(app).get("/v1/cards").expect(httpStatus.OK);

      expect(res.body).toEqual({
        response: expect.any(Array),
        status: true,
      });
    });
  });

  describe("POST /v1/card", () => {
    let newAccount;

    beforeEach(() => {
      newAccount = {
        name: "John Doe",
        card_number: "79927398713",
        limit: 12000,
      };
    });

    test("should return 200 and successfully create new card account if data is ok", async () => {
      const res = await request(app)
        .post("/v1/card")
        .send(newAccount)
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        response: expect.anything(),
        status: true,
      });
    });

    test("should return 400 error if card_number is invalid", async () => {
      newAccount.card_number = "invalidNumber";
      const res = await request(app)
        .post("/v1/card")
        .send(newAccount)
        .expect(httpStatus.BAD_REQUEST);

      expect(res.body).toEqual({
        error: '"card_number" is not valid',
        status: false,
      });
    });

    test("should return 400 error if name length is more than 30", async () => {
      newAccount.name = "this is a dummy name which is very lengthy";
      const res = await request(app)
        .post("/v1/card")
        .send(newAccount)
        .expect(httpStatus.BAD_REQUEST);

      expect(res.body).toEqual({
        error: '"name" length must be less than or equal to 30 characters long',
        status: false,
      });
    });
  });
});
