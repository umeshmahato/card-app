const express = require("express");
const audit = require("express-requests-logger");
const cookieParser = require("cookie-parser");
const cors = require("cors");

var indexRouter = require("./routes/index");

var app = express();
app.use(cors({ origin: "*" }));
app.use(audit());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use("/v1", indexRouter);

module.exports = app;
