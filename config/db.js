const Datastore = require("nedb");
const db = new Datastore({
  filename: process.env.CONFIG_DBNAME,
  autoload: true,
});
module.exports = db;
