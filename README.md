# NodeJS App for Credit Card accounts opening and listing


Tech Strategy:
```sh
Framework: NodeJS
Data storage: nedb (embedded)
```

API endpoints:

```sh
Account List API 
GET /v1/cards

Create Account API 
POST /v1/card

```

Environment variables:

```sh
1. CONFIG_PORT >> Port to start server in 
2. CONFIG_DBNAME >> Database name to store data
```

Install it and run:

```sh
yarn
yarn start
```

Install it and execute test cases:

```sh
yarn
yarn test
```
